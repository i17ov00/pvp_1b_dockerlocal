# Stacks Docker

Plusieurs alternatives sont disponibles dans ce repo.

Une seule stack ne peut être lancé en même temps.

Le principe est toujours le même :
- se placer dans le dossier de la stack
- lancer `docker-compose up -d`

Pour changer de stack bien faire un `docker-compose down` sur l'ancienne stack.

## Requirements

### FRONT STEP

Nécessite de builder `pvp_1b_stepfrontend` en mode prod avant de builder l'image nginx.

Pour cela il faut avoir l'image suivante en local:
`dtr.docker.si2m.tec/tools-store/usil-nginx-perl-run:1.15.0`

```shell
npm run build:prod

docker build -t pvp_1b_stepfrontend:0.0.13-SNAPSHOT .
```

### BACK STEP

Nécessite de builder `pvp_1b_stepbackend` en mode prod avant de builder l'image java.

Pour cela il faut avoir l'image suivante en local:
`dtr.docker.si2m.tec/tools-store/usil-openjdk-11-run:1.0.0`


```shell
mvn -Dmaven.test.skip=true -Dfile.encoding=UTF-8 package

docker build -t pvp_1b_stepbackend:0.0.13-SNAPSHOT .
```

### Tarificateur

Cf Teams pour les images


## stack-front-nginx

Contient:
- nginx front

Nécessite de builder `pvp_1b_stepfrontend` en mode prod avant de builder l'image nginx.

Pour cela il faut avoir l'image suivante en local:
`dtr.docker.si2m.tec/tools-store/usil-nginx-perl-run:1.15.0`

```shell
npm run build:prod

docker build -t pvp_1b_stepfrontend:0.0.13-SNAPSHOT .
```

Cette stack permet de tester le fléchage.

## stack-full

Contient:
- bdd mysql mutualisée pour tarificateur-santeindiv et tarificateur-documents
- tarificateur-santeindiv
- tarificateur-documents
- bdd pour stepbackend
- stepbackend
- serveur smtp

Il est possible de builder l'image stepbackend en local si on a l'image:
`dtr.docker.si2m.tec/tools-store/usil-openjdk-11-run:1.0.0`


```shell
mvn -Dmaven.test.skip=true -Dfile.encoding=UTF-8 package

docker build -t pvp_1b_stepbackend:0.0.13-SNAPSHOT .
```

## stack-mock

Contient:
- mock-server
- serveur smtp
- bdd pour stepbackend
- stepbackend

Il est possible de builder l'image stepbackend en local si on a l'image:
`dtr.docker.si2m.tec/tools-store/usil-openjdk-11-run:1.0.0`

## stack-mock-no-back

Contient:
- mock-server
- serveur smtp
- bdd pour stepbackend
