/* changelog-0.0.12 */
CREATE TABLE `demande_souscription`
(
    `id`                INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `guid`              CHAR(36)         NOT NULL,
    `produit`           VARCHAR(100)     NOT NULL DEFAULT '',
    `payload`           JSON             NOT NULL DEFAULT '',
    `tarifs`            JSON,
    `email`             VARCHAR(250)              DEFAULT NULL,
    `date_modification` TIMESTAMP        NOT NULL DEFAULT NOW(),
    `date_creation`     DATETIME         NOT NULL DEFAULT NOW(),
    `statut`            VARCHAR(32)      NOT NULL DEFAULT 'BROUILLON',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_guid` (guid),
    INDEX `Index_produit` (produit),
    INDEX `Index_statut` (statut)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE `selligent_session_commercial`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `cd_personne`               VARCHAR(45)      NOT NULL DEFAULT '',
    `nrid_personne`             VARCHAR(45)      NOT NULL DEFAULT '',
    `id_commercial`             VARCHAR(45)      NOT NULL DEFAULT '',
    `guid_demande_souscription` CHAR(36)         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_guid_demande_souscription` (`guid_demande_souscription`),
    CONSTRAINT `FK_selligent_session_commercial_demande_souscription`
        FOREIGN KEY `FK_selligent_session_commercial_demande_souscription` (`guid_demande_souscription`)
            REFERENCES `demande_souscription` (`guid`)
            ON DELETE CASCADE
            ON UPDATE RESTRICT
)
    ENGINE = InnoDB
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

CREATE TABLE `devis_selligent`
(
    `id`                      INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_demande_souscription` INTEGER UNSIGNED NOT NULL DEFAULT 0,
    `date_envoi`              DATETIME                  DEFAULT NULL,
    `sent`                    TINYINT          NOT NULL DEFAULT 0,
    `nb_try`                  INTEGER UNSIGNED NOT NULL DEFAULT 0,
    `erreur`                  longblob,
    PRIMARY KEY (`id`),
    UNIQUE `Index_unique_demande_souscription` (`id_demande_souscription`),
    CONSTRAINT `FK_devis_selligent_demande_souscription`
        FOREIGN KEY `FK_devis_selligent_demande_souscription` (`id_demande_souscription`)
            REFERENCES `demande_souscription` (`id`)
            ON DELETE CASCADE
            ON UPDATE RESTRICT
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 80000000
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

CREATE TABLE `mail_sortant`
(
    `id`            integer unsigned NOT NULL AUTO_INCREMENT,
    `destinataire`  varchar(255)     NOT NULL DEFAULT '',
    `emetteur`      varchar(255)     NOT NULL DEFAULT '',
    `objet`         varchar(255)     NOT NULL DEFAULT '',
    `corps`         mediumtext       NOT NULL,
    `creation_date` datetime         NOT NULL DEFAULT NOW(),
    `priority`      smallint(6)               DEFAULT '1',
    `date_envoi`    datetime                  DEFAULT NULL,
    `erreur`        longblob,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE `mail_sortant_piece_jointe`
(
    `id`                   integer unsigned NOT NULL AUTO_INCREMENT,
    `contenu_piece_jointe` mediumblob,
    `id_mail_sortant`      integer unsigned DEFAULT NULL,
    `nom_contenu`          varchar(256)      DEFAULT NULL,
    `type_contenu`         varchar(64)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_PJ_MAIL_SORTANT` (`id_mail_sortant`),
    CONSTRAINT `FK_PJ_MAIL_SORTANT` FOREIGN KEY (`id_mail_sortant`) REFERENCES `mail_sortant` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE `client_entreprise`
(
    `id`                 INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `carte_tp`           VARCHAR(10)      NOT NULL DEFAULT '',
    `siren`              CHAR(9)          NOT NULL DEFAULT '',
    `regime_accueil`     TINYINT UNSIGNED NOT NULL DEFAULT 0,
    `organisme_assureur` VARCHAR(50)      NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_carte_tp` (carte_tp),
    INDEX `Index_siren` (siren)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

-- Modele pour le task scheduler Shedlock
CREATE TABLE `shedlock`
(
    name       VARCHAR(64)  NOT NULL,
    lock_until TIMESTAMP(3) NOT NULL,
    locked_at  TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    locked_by  VARCHAR(255) NOT NULL,
    PRIMARY KEY (name)
);


create table `justificatif`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `guid_demande_souscription` CHAR(36)         NOT NULL,
    `beneficiaire`              VARCHAR(36)      NOT NULL, -- ASSURE_PRINCIPAL, CONJOINT, ENFANT[1], ENFANT[2], etc..
    `nomfichier_original`         VARCHAR(256)     NOT NULL,
    `contenu`                   mediumblob       NOT NULL,
    `type`                      VARCHAR(256),
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_justificatif_demande_souscription`
        FOREIGN KEY `FK_justificatif_demande_souscription` (`guid_demande_souscription`)
            REFERENCES `demande_souscription` (`guid`)
            ON DELETE CASCADE
            ON UPDATE RESTRICT
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

/* changelog-0.0.13 */
create table `demande_souscription_history`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `guid_demande_souscription` CHAR(36)         NOT NULL,
    `statut_before`             VARCHAR(32)      NOT NULL,
    `statut_after`              VARCHAR(32)      NOT NULL,
    `date_modification`         TIMESTAMP        NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_demande_souscription_history`
        FOREIGN KEY `FK_demande_souscription_history` (`guid_demande_souscription`)
            REFERENCES `demande_souscription` (`guid`)
            ON DELETE CASCADE
            ON UPDATE RESTRICT
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

create table `cartetp_entreprise`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_rcu` VARCHAR(16)         NOT NULL,
    `siren`             VARCHAR(16),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_id_rcu_cartetp_entreprise` (`id_rcu`)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

create table `entreprise_regime_accueil`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `siren`             VARCHAR(16)      NOT NULL,
    `entreprise` VARCHAR(256)         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_siren_entreprise_regime_accueil` (`siren`)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

create table `entreprise_sensible`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `siren`             VARCHAR(16)      NOT NULL,
    `entreprise` VARCHAR(256)         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_siren_entreprise_sensible` (`siren`)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

create table `departement_assureur`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `code_postal`             VARCHAR(5)      NOT NULL,
    `organisme_assureur` VARCHAR(256)         NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_code_postal_departement_assureur` (`code_postal`)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE `signature_enveloppe`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_enveloppe`              VARCHAR(50)      NOT NULL DEFAULT '',
    `guid_demande_souscription` CHAR(36)         NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_esign_enveloppe_guid` FOREIGN KEY `FK_esign_enveloppe_guid` (`guid_demande_souscription`)
        REFERENCES `demande_souscription` (`guid`)
        ON DELETE CASCADE
        ON UPDATE RESTRICT
)
    ENGINE = InnoDB
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

CREATE TABLE `contrat`
(
    `id`                        INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `numero_contrat`            VARCHAR(45)      NOT NULL DEFAULT '',
    `guid_demande_souscription` CHAR(36)         NOT NULL DEFAULT '',
    `produit`                   VARCHAR(100)     NOT NULL DEFAULT '',
    `code_formule`              VARCHAR(100)     NOT NULL DEFAULT '',
    `id_signature_enveloppe`    INTEGER UNSIGNED,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_numero_contrat` (`numero_contrat`),
    INDEX `Index_enveloppe_signature` USING BTREE (`id_signature_enveloppe`),
    CONSTRAINT `FK_contrat_signature_enveloppe` FOREIGN KEY `FK_contrat_signature_enveloppe` (`id_signature_enveloppe`)
        REFERENCES `signature_enveloppe` (`id`)
        ON DELETE CASCADE
        ON UPDATE RESTRICT,
    CONSTRAINT `FK_contrat_demande_souscription` FOREIGN KEY `FK_contrat_demande_souscription` (`guid_demande_souscription`)
        REFERENCES `demande_souscription` (`guid`)
        ON DELETE CASCADE
        ON UPDATE RESTRICT
)
    ENGINE = InnoDB
    CHARACTER SET utf8
    COLLATE utf8_general_ci;

/* changelog-0.0.14 */
ALTER TABLE `signature_enveloppe`
    ADD COLUMN `statut` VARCHAR(50) NOT NULL DEFAULT 'EN_ATTENTE' AFTER `guid_demande_souscription`,
    ADD COLUMN `date_creation` DATETIME NOT NULL DEFAULT NOW() AFTER `statut`,
    ADD COLUMN `date_modification` TIMESTAMP NOT NULL DEFAULT NOW() AFTER `date_creation`,
    ADD INDEX `Index_statut`(`statut`);

ALTER TABLE `signature_enveloppe` ADD UNIQUE INDEX `Index_id_enveloppe`(`id_enveloppe`);

ALTER TABLE `contrat`
    DROP FOREIGN KEY `FK_contrat_signature_enveloppe`,
    DROP INDEX  `Index_enveloppe_signature`;

ALTER TABLE `contrat` MODIFY COLUMN `id_signature_enveloppe` VARCHAR(50);

ALTER TABLE `contrat`
    ADD CONSTRAINT `FK_contrat_signature_enveloppe`
        FOREIGN KEY `FK_contrat_signature_enveloppe` (`id_signature_enveloppe`)
            REFERENCES `signature_enveloppe` (`id_enveloppe`)
            ON DELETE SET NULL
            ON UPDATE SET NULL;

DELETE FROM `justificatif`;

ALTER TABLE `justificatif`
    ADD guid CHAR(36) NOT NULL,
    ADD UNIQUE INDEX `Index_guid_enveloppe`(`guid`),
    DROP COLUMN `contenu`;

-- Replace FK id demande to guid demande

ALTER TABLE `devis_selligent`
    ADD `guid_demande_souscription` CHAR(36) NOT NULL;

ALTER TABLE `devis_selligent` DROP FOREIGN KEY `FK_devis_selligent_demande_souscription`;

ALTER TABLE `devis_selligent` DROP KEY `Index_unique_demande_souscription`;

ALTER TABLE `devis_selligent` DROP COLUMN `id_demande_souscription`;

ALTER TABLE `devis_selligent`
    ADD CONSTRAINT `FK_devis_selligent_demande_souscription`
        FOREIGN KEY `FK_devis_selligent_demande_souscription` (`guid_demande_souscription`)
            REFERENCES `demande_souscription` (`guid`)
            ON DELETE CASCADE
            ON UPDATE RESTRICT;

CREATE UNIQUE INDEX `Index_guid_demande_souscription`
    ON `devis_selligent` (`guid_demande_souscription`);

/* changelog-0.0.15 */
ALTER TABLE `devis_selligent`
 ADD `cd_personne`               VARCHAR(45)      DEFAULT NULL,
 ADD `nrid_personne`             VARCHAR(45)      DEFAULT NULL;

-- Demande souscription
ALTER TABLE `demande_souscription` ADD COLUMN `date_effet` DATETIME AFTER `statut`,
                                   ADD INDEX `Index_date_effet`(`date_effet`);

/* changelog-0.0.16 */

/* changelog-0.0.17 */
-- PPROD
-- Ajoute une colonne assureur a la table cartetp
alter table cartetp_entreprise
    add assureur varchar(16) default 'MMH' not null;

update cartetp_entreprise
set assureur='MMH';

-- Ajoute une colonne entitee a la table selligent_session_commercial

alter table selligent_session_commercial
    add entitee varchar(16) default 'MMH' not null;

update selligent_session_commercial
set entitee='MMH';

update demande_souscription set payload = JSON_SET(payload, "$.domainName", "MMH") where produit='PSP';
update demande_souscription set payload = JSON_SET(payload, "$.organismeDistributeur", "MMH") where produit='PSP';

update demande_souscription set tarifs = JSON_REMOVE(tarifs, "$.reductionCommercial.reductionZoneGeographique") where produit = 'PSP';
update demande_souscription set tarifs = JSON_REMOVE(tarifs, "$.reductionCommercial.reductionActifs") where produit = 'PSP';
update demande_souscription set tarifs = JSON_SET(tarifs, "$.reductionCommercial.reductionSouplesseCommerciale", 0) where produit = 'PSP';

-- RECETTE
UPDATE `departement_assureur` set organisme_assureur='MMH' where code_postal='43' or code_postal='42';

DELETE FROM cartetp_entreprise;
ALTER TABLE `cartetp_entreprise`
    ADD COLUMN `id_pleiade` VARCHAR(16) AFTER `id_rcu`,
    ADD INDEX `Index_id_pleiade`(`id_pleiade`);

DELETE FROM `entreprise_regime_accueil`;
DELETE FROM entreprise_sensible;

/* changelog-2.0.0 */
-- Ajoute une attribute json "versionProduit" au payload  dans la table demande_souscription
update demande_souscription
set payload = JSON_SET(payload, "$.versionProduit", "PSPV1")
where produit = 'PSP';

CREATE TABLE `parametre`
(
    `id`    INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    `code`  VARCHAR(200)     NOT NULL,
    `value` JSON             NOT NULL DEFAULT '',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `Index_code` (code)
) ENGINE = InnoDB
  CHARACTER SET utf8
  COLLATE utf8_general_ci;

INSERT INTO `parametre` VALUES (1,'promotion-psp','{\"enable\":true,\"promotion\":\"« Bénéficiez de 2 mois de cotisations à 50% ! »****\",\"asterisque\":\"**** Vous pouvez bénéficier d’une réduction de 50% sur 2 mois consécutifs de cotisations pour tout nouveau contrat Pack Santé Particuliers / formule Etendu ou Intégral, souscrit entre le 01/09/2021 et le 31/01/2022. Offre réservée aux nouveaux clients et conditionnée à la remise à l’organisme assureur - avant le 31 janvier 2022 -  de l’ensemble des pièces justificatives demandées lors de la souscription du contrat ; La réduction sera appliquée à l’issue de la période de renonciation au contrat et sur les deux mois consécutifs suivants, pour l’assuré et l’ensemble de ses ayant-droits bénéficiaires. Votre appel de cotisations vous informera précisément de l’application effective de cette réduction. Réduction non cumulable avec toute autre remise ou promotion.\"}');

/* changelog-2.1.0 */
# PVP-666
ALTER TABLE `selligent_session_commercial` ADD COLUMN `email_commercial` VARCHAR(250) AFTER `id_commercial`;
ALTER TABLE `mail_sortant` ADD COLUMN `destinataire_cc` VARCHAR(255) AFTER `emetteur`,
                           ADD COLUMN `destinataire_cci` VARCHAR(255) AFTER `destinataire_cc`;

# PVP-173 gda
CREATE TABLE `demande_souscription_gda`
(
    `id`                        INTEGER UNSIGNED                                    NOT NULL AUTO_INCREMENT,
    `guid_demande_souscription` CHAR(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    `statut`                    VARCHAR(45)                                         NOT NULL DEFAULT '',
    `id_dossier`                VARCHAR(45)                                                  DEFAULT NULL,
    `date_modification`         timestamp                                           NOT NULL DEFAULT current_timestamp(),
    `date_creation`             DATETIME                                            NOT NULL DEFAULT 0,
    `notification_failed`       TINYINT UNSIGNED                                    NOT NULL DEFAULT 0,
    `notification_erreur`       LONGBLOB                                                     DEFAULT NULL,
    `notification_nb_try`       INTEGER UNSIGNED                                    NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    INDEX `Index_statut` (`statut`),
    UNIQUE INDEX `Index_guid_demande_souscription` (`guid_demande_souscription`),
    CONSTRAINT `FK_demande_souscription_gda` FOREIGN KEY `FK_demande_souscription_gda` (`guid_demande_souscription`)
        REFERENCES `demande_souscription` (`guid`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
